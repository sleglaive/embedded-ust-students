#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 10:29:17 2019

This scripts records 10-second audio file using sounddevice, 
computes a log-Mel spectrogram, and predicts the tags using 
a trained CNN.

@author: sleglaive
"""

import os
import oyaml as yaml
import numpy as np
from keras.models import model_from_json
import mel_features
import sounddevice as sd
from scipy.io.wavfile import write

# Run python3 -m sounddevice
# and choose the proper device
sd.default.device = 7

#%%============================================================================
############################# PATH TO YOUR NETWORK ############################
###============================================================================
    
model_architecture_file = 'your/path/to/model_architecture.json'
model_weights_file = 'your/path/to/best_model_weights.h5'

#%%
# =============================================================================
# Load  taxonomy
# =============================================================================

# Load annotations and taxonomy
taxonomy_file =  './dcase-ust-taxonomy.yaml'
with open(taxonomy_file, 'r') as f:
    taxonomy = yaml.load(f, Loader=yaml.Loader)

# get list of coarse labels from taxonomy
labels = [v for k,v in taxonomy['coarse'].items()]

#%%
# =============================================================================
# Capture audio and compute log-Mel spectrogram
# =============================================================================

# add your code here

#%%
# =============================================================================
# Load model
# =============================================================================

# add your code here

#%%
# =============================================================================
# Predict
# =============================================================================

# add your code here