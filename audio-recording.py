#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 11:41:12 2019

This scripts records an audio file using sounddevice.

@author: sleglaive
"""

import tempfile
import queue
import sys
import sounddevice as sd
import soundfile as sf
from scipy.io.wavfile import write
import numpy  # Make sure NumPy is loaded before it is used in the callback

filename = tempfile.mktemp(prefix='recording_', suffix='.wav', dir='')
device = 7 # 'input device (numeric ID or substring)' Run "python3 -m sounddevice" to identify the device.
sd.default.device = 7

samplerate = 44100
channels = 1
subtype = "PCM_24"

# record a fixed or infinite duration
recording_duration = "fixed" # "fixed" or "infinite"

if recording_duration == "fixed":
    
    seconds = 10  # Duration of recording

    print('Recording started')
    y = sd.rec(int(seconds * samplerate), samplerate=samplerate, channels=1)
    sd.wait()  # Wait until recording is finished
    print('\nRecording finished: ' + repr(filename))
    write(filename, samplerate, y)  # Save as WAV file 

elif recording_duration == "infinite":

    q = queue.Queue()

    def callback(indata, frames, time, status):
        """This is called (from a separate thread) for each audio block."""
        if status:
            print(status, file=sys.stderr)
        q.put(indata.copy())

    try:
        # Make sure the file is opened before recording anything:
        with sf.SoundFile(filename, mode='x', samplerate=samplerate,
                        channels=channels, subtype=subtype) as file:
            with sd.InputStream(samplerate=samplerate, device=device,
                                channels=channels, callback=callback):
                print('#' * 80)
                print('press Ctrl+C to stop the recording')
                print('#' * 80)
                while True:
                    audio_block = q.get()
                    file.write(audio_block)
    except KeyboardInterrupt:
        print('\nRecording finished: ' + repr(filename))
